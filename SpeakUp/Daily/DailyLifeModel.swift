//
//  DailyLifeModel.swift
//  SpeakUp
//
//  Created by wong tze wai leah on 13/10/2018.
//  Copyright © 2018 ee. All rights reserved.
//

import Foundation

class DailyLifeModel {
    
    var displayString: String?
    var speakText: String?
    var r_index: Float?
    var b_index: Float?
    var g_index: Float?
    
    
    init(displayString: String?, speakText: String?, r_index: Float?, b_index: Float?, g_index: Float?) {
        self.displayString = displayString
        self.speakText = speakText
        self.r_index = r_index
        self.b_index = b_index
        self.g_index = g_index
    }
}
