//
//  tagSoundViewController.swift
//  SpeakUp
//
//  Created by Ka Chun Fung on 18/11/2018.
//  Copyright © 2018 cpdev. All rights reserved.
//

import UIKit
import CoreData
import AVFoundation

class tagSoundViewController: UIViewController, UITextFieldDelegate {
    
    
    @IBOutlet weak var tagNameField: UITextField!
    @IBOutlet weak var tagContenField: UITextView!
    
    @IBAction func cancelAction(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }
    
    @IBAction func soundAction(_ sender: Any) {
        
       /*******************************
        Add your speak code here
        ******************************/
        

        let context = (UIApplication.shared.delegate as! AppDelegate).persistentContainer.viewContext
        
        
        do{
            let fetchRequest: NSFetchRequest<Tag> = Tag.fetchRequest()
            fetchRequest.predicate = NSPredicate(format: "name == %@", tagNameField.text!)
            let record = try context.fetch(fetchRequest)
            let appDelegate = (UIApplication.shared.delegate as! AppDelegate)
            var found = false
            
            if record.count > 0{
                
                for tag in record {
                    tag.name = tagNameField.text
                    tag.content = tagContenField.text
                    found = true
                }
                if found {
                    appDelegate.saveContext()
                    dismiss(animated: true, completion: nil)
                }
            }
        }catch{
            print("Fetching Failed")
        }
        
        speak(speaktext: tagContenField.text!)
    }
    
    func speak(speaktext: String){
        let speakTalk   = AVSpeechSynthesizer()
        let speakMsg    = AVSpeechUtterance(string: speaktext )
        
        speakMsg.voice  = AVSpeechSynthesisVoice(language: "zh-HK")
        speakMsg.pitchMultiplier = 1.2
        speakMsg.rate   = 0.5
        
        speakTalk.speak(speakMsg)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        getData()
    }
    
    func getData(){
        let key = UserDefaults.standard.object(forKey: "tagName")
        UserDefaults.standard.set("", forKey: "tagName")
        let context = (UIApplication.shared.delegate as! AppDelegate).persistentContainer.viewContext
        
        do{
            let fetchRequest: NSFetchRequest<Tag> = Tag.fetchRequest()
            fetchRequest.predicate = NSPredicate(format: "name == %@", key as! CVarArg)
            let record = try context.fetch(fetchRequest)
            
            if record.count > 0{
                var found = false
                var tagName = ""
                var content = ""
                
                for tag in record {
                    tagName = tag.name!
                    content = tag.content!
                    found = true
                }
                if found {
                    tagNameField.text = tagName
                    tagContenField.text = content
                }
            }
        }catch{
            print("Fetching Failed")
        }
        
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return false
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        view.endEditing(true)
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
