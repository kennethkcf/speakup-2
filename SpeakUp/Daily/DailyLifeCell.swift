//
//  DailyLifeCell.swift
//  SpeakUp
//
//  Created by cpdev on 14/11/2018.
//  Copyright © 2018 cpdev. All rights reserved.
//

import UIKit

class DailyLifeCell: UITableViewCell {

    @IBOutlet var cellButton: UIButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        cellButton.titleLabel?.numberOfLines = 0
        cellButton.titleLabel?.font =  UIFont(name: "AvenirNext-Medium", size: 20)!
        cellButton.setTitleColor(UIColor.black, for: UIControl.State())
        cellButton.layer.cornerRadius = 12
        cellButton.clipsToBounds = true
        
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    

}
