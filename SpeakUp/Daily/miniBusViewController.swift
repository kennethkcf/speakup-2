//
//  miniBusViewController.swift
//  SpeakUp
//
//  Created by Ka Chun Fung on 18/11/2018.
//  Copyright © 2018 cpdev. All rights reserved.
//

import UIKit
import AVFoundation

class miniBusViewController: UIViewController, UIPickerViewDelegate, UIPickerViewDataSource{
    
    let sent: [String] = ["有落", "轉灣有落", "前面有落", "燈位有落", "巴士站有落"]
    var speakSent:String = ""

    @IBAction func cancelAction(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }
    
    @IBAction func speakAction(_ sender: Any) {
        
        speak(speaktext: speakSent)
        
    }
    
    func speak(speaktext: String){
        let speakTalk   = AVSpeechSynthesizer()
        let speakMsg    = AVSpeechUtterance(string: speaktext )
        
        speakMsg.voice  = AVSpeechSynthesisVoice(language: "zh-HK")
        speakMsg.pitchMultiplier = 1.2
        speakMsg.rate   = 0.5
        
        speakTalk.speak(speakMsg)
    }
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return sent.count
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return sent[row]
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        speakSent = sent[row]
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
