//
//  FirstViewController.swift
//  SpeakUp
//
//  Created by cpdev on 14/11/2018.
//  Copyright © 2018 cpdev. All rights reserved.
//

import UIKit

class DailyLifeController: UIViewController, UITabBarDelegate, UITableViewDataSource {

    @IBOutlet var navBar: UINavigationBar!
    @IBOutlet var miniBusButton: UIButton!
    @IBOutlet var tableView: UITableView!
    
    var items: [Tag] = []
    
    @IBAction func cellButtonAction(_ sender: Any) {
        let btn = sender as! UIButton
        let cell = btn.superview?.superview as! DailyLifeCell
        UserDefaults.standard.set("", forKey: "tagName")
        UserDefaults.standard.set(cell.cellButton.currentTitle, forKey: "tagName")
        performSegue(withIdentifier: "cellContent", sender: self)
    }
    
    @IBAction func miniBusAction(_ sender: Any) {
        performSegue(withIdentifier: "miniBus", sender: self)
    }
    
    @IBAction func addButtonAction(_ sender: Any) {
        performSegue(withIdentifier: "dailyLifeMain", sender: self)
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        miniBusButton.titleLabel?.font =  UIFont(name: "AvenirNext-Medium", size: 20)!
        miniBusButton.setTitleColor(UIColor.black, for: UIControl.State())
        miniBusButton.layer.cornerRadius = 12
        miniBusButton.clipsToBounds = true
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return items.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell: DailyLifeCell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath) as! DailyLifeCell

        let item = items[indexPath.row]
        
        cell.cellButton.tag = indexPath.row
        cell.cellButton.setTitle(item.name, for: UIControl.State())
        cell.cellButton.backgroundColor = UIColor.init(red: CGFloat(item.r_index), green: CGFloat(item.g_index), blue: CGFloat(item.b_index), alpha: 0.5)
        cell.cellButton.addTarget(self, action: #selector(self.cellButtonAction(_:)), for: .touchUpInside)
        cell.selectionStyle = UITableViewCell.SelectionStyle.none

        return cell
    }
    
    func getData(){
        let context = (UIApplication.shared.delegate as! AppDelegate).persistentContainer.viewContext
        
        do{
            items = try context.fetch(Tag.fetchRequest())
        }catch {
            print("Fetching Failed")
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        getData()
        
        tableView.reloadData()
    }
    
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        let context = (UIApplication.shared.delegate as! AppDelegate).persistentContainer.viewContext
        
        if editingStyle == .delete{
            let item = items[indexPath.row]
            context.delete(item)
            
            (UIApplication.shared.delegate as! AppDelegate).saveContext()
            
            do{
                items = try context.fetch(Tag.fetchRequest())
            }catch {
                print("Fetching Failed")
            }
        }
        
        tableView.reloadData()
    }
    
    @objc func test (_ sender: UIButton){
        print("OK")
    }
    
}

