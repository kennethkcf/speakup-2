//
//  EmergencyViewController.swift
//  SpeakUp
//
//  Created by Frankie on 17/11/2018.
//  Copyright © 2018 cpdev. All rights reserved.
//

import Foundation
import UIKit
import SCLAlertView
import MessageUI
import CoreLocation
import AVFoundation

class EmergencyViewController: UIViewController, MFMessageComposeViewControllerDelegate, CLLocationManagerDelegate, UITextFieldDelegate {
    
    @IBOutlet var policeButton: UIButton!
    @IBOutlet var fireButton: UIButton!
    @IBOutlet var emergencyButton: UIButton!
    @IBOutlet var alertButton: UIButton!
    @IBOutlet var ambulanceButton: UIButton!
    
    var phoneNumer = "33333333"
    var locationManager: CLLocationManager = CLLocationManager()
    var currentLatitude = ""
    var currentLongitude = ""
    
    var startLocation: CLLocation!
    var bombSoundEffect: AVAudioPlayer?
    
    func messageComposeViewController(_ controller: MFMessageComposeViewController, didFinishWith result: MessageComposeResult) {
        controller.dismiss(animated: true, completion: nil)
    }
    
    func displayMessageInterface(tel: String, message: String) {
        let composeVC = MFMessageComposeViewController()
        composeVC.messageComposeDelegate = self
        
        // Configure the fields of the interface.
        composeVC.recipients = [tel]
        composeVC.body = message
        
        // Present the view controller modally.
        if MFMessageComposeViewController.canSendText() {
            self.present(composeVC, animated: true, completion: nil)
        } else {
            print("Can't send messages.")
        }
    }
    
    @IBAction func police(_ sender: Any) {
        
        let alertView = SCLAlertView()
        let alertViewIcon = UIImage(named: "police")
        let url: NSURL = URL(string: "tel://11111111")! as NSURL
        
        alertView.addButton("Call") {
            UIApplication.shared.open(url as URL, options: [:], completionHandler: nil)
        }
        
        alertView.addButton("SMS") {
            
            self.displayMessageInterface(tel: "11111111", message: "I need a police service \nMy location: (Latitude: \(self.currentLatitude) Longitude: \(self.currentLongitude))")
        }
       
        alertView.showInfo("Police", subTitle: "Choose Your Service", closeButtonTitle: "Cancel", circleIconImage: alertViewIcon)
    }
    
    @IBAction func ambulance(_ sender: Any) {
        let alertView = SCLAlertView()
        let alertViewIcon = UIImage(named: "ambulance")
        let url: NSURL = URL(string: "tel://11111111")! as NSURL
        
        alertView.addButton("Call") {
            UIApplication.shared.open(url as URL, options: [:], completionHandler: nil)
        }
        
        alertView.addButton("SMS") {
            
            self.displayMessageInterface(tel: "11111111", message: "I need an ambulance service \nMy location: (Latitude: \(self.currentLatitude) Longitude: \(self.currentLongitude))")
        }
        
        alertView.showInfo("Ambulance", subTitle: "Choose Your Service", closeButtonTitle: "Cancel", circleIconImage: alertViewIcon)
    }
    
    @IBAction func fire(_ sender: Any) {
        
        let alertView = SCLAlertView()
        let alertViewIcon = UIImage(named: "fire")
        let url: NSURL = URL(string: "tel://22222222")! as NSURL
        
        alertView.addButton("Call") {
            UIApplication.shared.open(url as URL, options: [:], completionHandler: nil)
        }
        
        alertView.addButton("SMS") {
            self.displayMessageInterface(tel: "22222222", message: "I need a fire service \nMy location: (Latitude: \(self.currentLatitude) Longitude: \(self.currentLongitude))")
        }
        
        alertView.showError("Fire", subTitle: "Choose Your Service", closeButtonTitle: "Cancel", circleIconImage: alertViewIcon)
    }
    
    @IBAction func emergency(_ sender: Any) {
        
        let alertView = SCLAlertView()
        let alertViewIcon = UIImage(named: "emergency")
        
        alertView.addButton("Call") {
            
            let callNumber = "\("tel://")\(self.phoneNumer)"
            print(callNumber)
            print(self.phoneNumer)
            let url: NSURL = URL(string: callNumber)! as NSURL
            UIApplication.shared.open(url as URL, options: [:], completionHandler: nil)
        }
        
        alertView.addButton("SMS") {
            self.displayMessageInterface(tel: self.phoneNumer, message: "I need help \nMy location: (Latitude: \(self.currentLatitude) Longitude: \(self.currentLongitude))")
        }
        
        alertView.addButton("Edit") {
            let alert = SCLAlertView()
            
            let phoneNum = alert.addTextField("phone number")
            phoneNum.keyboardType = UIKeyboardType.numberPad
            
            alert.addButton("Enter") {
                self.phoneNumer = phoneNum.text!
                
                let defaults = UserDefaults.standard
                defaults.set(self.phoneNumer, forKey: "MyNum")
            }
            
            alert.showEdit("Edit", subTitle: "Input new phone number", closeButtonTitle: "Cancel")
        }
    
        alertView.showWarning("Emergency contact", subTitle: "Choose Your Service", closeButtonTitle: "Cancel", circleIconImage: alertViewIcon)
    }
    
    @IBAction func alert(_ sender: Any) {
        
        let appearance = SCLAlertView.SCLAppearance(
            showCloseButton: false // hide default button
        )
        let alertView = SCLAlertView(appearance: appearance)
        let alertViewIcon = UIImage(named: "attention")
        
        let path = Bundle.main.path(forResource: "ALARM.mp3", ofType:nil)!
        let url = URL(fileURLWithPath: path)
        
        do {
            bombSoundEffect = try AVAudioPlayer(contentsOf: url)
            bombSoundEffect?.play()
        } catch {
            // couldn't load file :(
        }
        
        alertView.addButton("Cancel"){
            self.bombSoundEffect?.stop()
        }
        
        
        alertView.showNotice("Alert", subTitle: "", circleIconImage: alertViewIcon)
        
        
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        
        buttonStyle(ButtonName: policeButton)
        buttonStyle(ButtonName: fireButton)
        buttonStyle(ButtonName: emergencyButton)
        buttonStyle(ButtonName: alertButton)
        buttonStyle(ButtonName: ambulanceButton)
        
        if let Num = UserDefaults.standard.string(forKey: "MyNum") {
            phoneNumer = Num
        }
        locationManager.desiredAccuracy = kCLLocationAccuracyBest
        locationManager.delegate = self
        locationManager.requestWhenInUseAuthorization()
        locationManager.startUpdatingLocation()
        
        
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        
        let latestLocation: AnyObject = locations[locations.count - 1]
        
        currentLatitude = String(format: "%.4f", latestLocation.coordinate.latitude)
        currentLongitude = String(format: "%.4f", latestLocation.coordinate.longitude)
        
    }
    
    func buttonStyle(ButtonName: UIButton){
        ButtonName.titleLabel?.font =  UIFont(name: "AvenirNext-Medium", size: 20)!
        ButtonName.setTitleColor(UIColor.black, for: UIControl.State())
        ButtonName.layer.cornerRadius = 12
        ButtonName.clipsToBounds = true
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return false
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        view.endEditing(true)
    }
}

