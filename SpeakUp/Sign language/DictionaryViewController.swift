//
//  DictionaryViewController.swift
//  SpeakUp
//
//  Created by Frankie on 16/11/2018.
//  Copyright © 2018 cpdev. All rights reserved.
//

import Foundation
import UIKit
import SCLAlertView
import WebKit

class DictionaryViewController: UIViewController {
    
    @IBOutlet var mWebView: WKWebView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        
        let mURL = URL(string:"https://www.handspeak.com/word/search")!
        let request = URLRequest(url: mURL)
        mWebView.load(request)
    }

}
