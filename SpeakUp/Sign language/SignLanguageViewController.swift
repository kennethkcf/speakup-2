//
//  SignLanguageViewController.swift
//  SpeakUp
//
//  Created by Frankie on 16/11/2018.
//  Copyright © 2018 cpdev. All rights reserved.
//

import Foundation
import UIKit
import SCLAlertView

class SignLanguageViewController: UIViewController {
    
    @IBOutlet var sadButton: UIButton!
    @IBOutlet var braveButton: UIButton!
    @IBOutlet var allRightButton: UIButton!
    @IBOutlet var helpButton: UIButton!
    @IBOutlet var loveButton: UIButton!
    @IBOutlet var likeButton: UIButton!
    @IBOutlet var awesomeButton: UIButton!
    @IBOutlet var dictionaryButton: UIButton!
    
    
    @IBOutlet var gifImage: UIImageView!
    @IBOutlet var closeButton: UIButton!
    @IBOutlet var ImageBar: UINavigationBar!
    
    @IBAction func allRight(_ sender: Any) {
        alertImage(imageName:"allRight", name:"Are you all right?")
    }
    
    @IBAction func help(_ sender: Any) {
        alertImage(imageName:"help", name:"Please help me")
    }
    
    @IBAction func like(_ sender: Any) {
        alertImage(imageName:"like", name:"Like")
    }
    
    @IBAction func love(_ sender: Any) {
        alertImage(imageName:"love", name:"Love")
    }
    
    @IBAction func awesome(_ sender: Any) {
        alertImage(imageName:"awesome", name:"Awesmoe")
    }
    
    @IBAction func brave(_ sender: Any) {
        alertImage(imageName:"brave", name:"Brave")
    }
    
    @IBAction func sad(_ sender: Any) {
        alertImage(imageName:"sad", name:"Sad")
    }
    
    
    func alertImage(imageName : String, name : String){
        
        let appearance = SCLAlertView.SCLAppearance(
            kWindowWidth: CGFloat(350),
            kWindowHeight: CGFloat(170),
            kTitleFont:UIFont(name: "HelveticaNeue", size: 30)!,
            //kTextFont:UIFont(name: "HelveticaNeue", size: 14)!,
            kButtonFont:UIFont(name: "HelveticaNeue-Bold", size: 20)!,
            
            showCircularIcon: true,
            contentViewCornerRadius : CGFloat(20)
        )
        
        let alertView = SCLAlertView(appearance: appearance)
        let alertViewIcon = UIImage(named: "hand_cursor")
        let image = UIImageView(frame: CGRect(x:0, y:0, width:350, height:170))
        image.loadGif(name: imageName)
        
        alertView.customSubview = image
        alertView.showInfo(name, subTitle: "", circleIconImage: alertViewIcon)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        
        buttonStyle(ButtonName: sadButton)
        buttonStyle(ButtonName: braveButton)
        buttonStyle(ButtonName: allRightButton)
        buttonStyle(ButtonName: awesomeButton)
        buttonStyle(ButtonName: loveButton)
        buttonStyle(ButtonName: likeButton)
        buttonStyle(ButtonName: helpButton)
        buttonStyle(ButtonName: dictionaryButton)
        
    }
    
    func buttonStyle(ButtonName: UIButton){
        ButtonName.titleLabel?.font =  UIFont(name: "AvenirNext-Medium", size: 20)!
        ButtonName.setTitleColor(UIColor.black, for: UIControl.State())
        ButtonName.layer.cornerRadius = 20
        ButtonName.clipsToBounds = true
    }
}
