//
//  AddTagViewController.swift
//  SpeakUp
//
//  Created by Ka Chun Fung on 15/11/2018.
//  Copyright © 2018 cpdev. All rights reserved.
//

import UIKit
import CoreData

class AddTagViewController: UIViewController, UITextFieldDelegate {



    @IBOutlet weak var tagContentField: UITextField!
    @IBOutlet weak var tagNameField: UITextField!
    
    @IBAction func cancelAction(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }
    
    @IBAction func addButtonAction(_ sender: Any) {
        let tagName: String = tagNameField.text!
        let tagContent: String = tagContentField.text!
        
        let r_index = Float.random(in: 0..<1)
        let g_index = Float.random(in: 0..<1)
        let b_index = Float.random(in: 0..<1)
        
        let context = (UIApplication.shared.delegate as! AppDelegate).persistentContainer.viewContext
        
        if !tagName.isEmpty && !tagContent.isEmpty {
            
            let tag = Tag(context: context)
            tag.name = tagName
            tag.content = tagContent
            tag.r_index = r_index
            tag.b_index = b_index
            tag.g_index = g_index
            
            (UIApplication.shared.delegate as! AppDelegate).saveContext()
            
            dismiss(animated: true, completion: nil)
            	
        }else  {
            
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return false
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        view.endEditing(true)
    }
}
