//
//  SettingViewController.swift
//  SpeakUp
//
//  Created by Tze Wai Wong on 18/11/2018.
//  Copyright © 2018 cpdev. All rights reserved.
//
import Foundation
import UIKit

class SettingViewController: UIViewController, UINavigationControllerDelegate, UIImagePickerControllerDelegate, UITextFieldDelegate {

    var imagePickedBlock: ((UIImage) -> Void)?
    @IBOutlet var editButton: UIButton!
    @IBOutlet var nameText: UITextField!
    @IBOutlet var emergencyTelText: UITextField!
    @IBOutlet var emergencyNameText: UITextField!
    @IBOutlet var addrText: UITextField!
    @IBOutlet var emailText: UITextField!
    @IBOutlet var genderText: UITextField!
    
    //let imageData = UIImagePNGRepresentation(imagePickedBlock)
    
    @IBAction func save(_ sender: Any) {
        
        UserDefaults.standard.set(self.emergencyTelText.text!, forKey: "MyNum")
        UserDefaults.standard.set(nameText.text!, forKey: "MyName")
        UserDefaults.standard.set(genderText.text!, forKey: "MyGender")
        UserDefaults.standard.set(emergencyNameText.text!, forKey: "MyEmergencyName")
        UserDefaults.standard.set(emailText.text!, forKey: "MyEmail")
        UserDefaults.standard.set(addrText.text!, forKey: "MyName")
        //UserDefaults.standard.set(imageData!, value(forKey: "MyIcon"))
  
        view.endEditing(true)
    }

    
    @IBAction func edit(_ sender: Any) {
        let actionSheet = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
        
        actionSheet.addAction(UIAlertAction(title: "Camera", style: .default, handler: { (alert:UIAlertAction!) -> Void in
            self.camera()
        }))
        
        actionSheet.addAction(UIAlertAction(title: "Gallery", style: .default, handler: { (alert:UIAlertAction!) -> Void in
            self.photoLibrary()
        }))
        
        actionSheet.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: nil))
        
        self.present(actionSheet, animated: true, completion: nil)
    }
    
    func camera()
    {
        if UIImagePickerController.isSourceTypeAvailable(.camera){
            let myPickerController = UIImagePickerController()
            myPickerController.delegate = self;
            myPickerController.sourceType = .camera
            self.present(myPickerController, animated: true, completion: nil)
        }
        
    }
    
    func photoLibrary()
    {
        
        if UIImagePickerController.isSourceTypeAvailable(.photoLibrary){
            let myPickerController = UIImagePickerController()
            myPickerController.delegate = self;
            myPickerController.sourceType = .photoLibrary
            self.present(myPickerController, animated: true, completion: nil)
        }
    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        nameText.delegate = self
        genderText.delegate = self
        emergencyTelText.delegate = self
        emergencyNameText.delegate = self
        emailText.delegate = self
        addrText.delegate = self
        
        nameText.resignFirstResponder()
        genderText.resignFirstResponder()
        emergencyTelText.resignFirstResponder()
        emergencyNameText.resignFirstResponder()
        emailText.resignFirstResponder()
        addrText.resignFirstResponder()
        
        
        emergencyTelText.text = UserDefaults.standard.string(forKey: "MyNum")
        nameText.text = UserDefaults.standard.string(forKey: "MyName")
        genderText.text = UserDefaults.standard.string(forKey: "MyGender")
        emergencyNameText.text = UserDefaults.standard.string(forKey: "MyEmergencyName")
        emailText.text = UserDefaults.standard.string(forKey: "MyEmail")
        addrText.text = UserDefaults.standard.string(forKey: "MyName")
        //imageData = UserDefaults.standard.data(forKey: "MyIcon")
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        self.dismiss(animated: true, completion: nil)
    }
    
    private func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        if let image = info[.originalImage] as? UIImage {
            self.imagePickedBlock?(image)
        }else{
            print("Something went wrong")
        }
        self.dismiss(animated: true, completion: nil)
    }
    

    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return false
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        view.endEditing(true)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

}

